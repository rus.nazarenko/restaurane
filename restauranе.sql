DROP TABLE IF EXISTS worker CASCADE;
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE IF EXISTS supplements CASCADE;
DROP TABLE IF EXISTS tasks CASCADE;
DROP TABLE IF EXISTS clients;
DROP TABLE IF EXISTS kitchens;
DROP EXTENSION IF EXISTS pgcrypto;
DROP TRIGGER IF EXISTS readiness_check ON tasks;

CREATE EXTENSION pgcrypto;


CREATE TABLE worker(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    full_name VARCHAR(50) UNIQUE,
    salary INT
);

INSERT INTO worker (full_name, salary) VALUES ('Ksusha Lunina', 10000),
('Elena Zorina', 12000),
('Maria Shatohina', 11000),
('Kirill Zabaranilov', 13000);


CREATE TABLE kitchens(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    kitchen_name VARCHAR(10) UNIQUE
);

INSERT INTO kitchens (kitchen_name) VALUES ( 'kitchen 1'),
( 'kitchen 2'),
( 'kitchen 3');


CREATE TABLE clients (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    full_name VARCHAR(50) UNIQUE,
    phone VARCHAR(20),
    address_street VARCHAR(50),
    address_building VARCHAR(10)
);

INSERT INTO clients (full_name, phone, address_street, address_building) VALUES ('Ivan Ivanov', '068-456-65-85', 'Mira', '166'),
('Petr Ivanov', '050-456-65-85', 'Pobedi', '40B'),
('Tatiana Kirillova', '096-456-65-85', 'Tovarishcheskaya', '5'),
('Julia Chaplian', '063-456-65-85', 'Soborniy', '70');


CREATE TABLE products(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    product_name VARCHAR(20) UNIQUE,
    price float,
    allergen BOOLEAN,
    contains_milk BOOLEAN,
    kitchen_name VARCHAR(10) REFERENCES kitchens(kitchen_name)
);

INSERT INTO products (product_name, price, allergen, contains_milk, kitchen_name) VALUES ('Pizza', 40, false, true, 'kitchen 1'),
('Sushi', 50, true, false, 'kitchen 2'),
('Hamburger', 30, false, true, 'kitchen 1'),
('French fries', 20, true, false, 'kitchen 3'),
('Big Mac', 35, false, false, 'kitchen 1');


CREATE TABLE supplements(
    id  UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    supplement_name VARCHAR(20) UNIQUE,
    price float,
    allergen BOOLEAN,
    contains_milk BOOLEAN
);

INSERT INTO supplements(supplement_name, price, allergen, contains_milk) VALUES ('Sauce', 10, true, false),
('Sesame', 15, true, true),
('Tomatoes', 20, false, false),
('Cheese', 30, true, true),
('Mushrooms', 10, false, false),
('Mayonnaise', 5, true, true);


CREATE TABLE tasks(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    number SERIAL,
    client_name VARCHAR(20) REFERENCES clients(full_name),
    lead_time timestamptz,

    product_name VARCHAR(20) REFERENCES products(product_name),
    product_quantity INT,
    supplement_name VARCHAR(20) REFERENCES supplements(supplement_name),
    supplement_quantity INT,

    worker_name VARCHAR(50) REFERENCES worker(full_name),
    completed boolean DEFAULT false,
    delivered boolean  DEFAULT false   
);


CREATE OR REPLACE FUNCTION readiness_check_func( ) RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.delivered=true) THEN
        IF (OLD.completed=false) THEN
            NEW.delivered=false;
            RETURN NEW;
        ELSIF (OLD.completed=true) THEN
            NEW.delivered=true;
            RETURN NEW;
        END IF;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER readiness_check BEFORE INSERT OR UPDATE
    ON tasks
    FOR EACH ROW
    EXECUTE PROCEDURE readiness_check_func();


----------------- To be completed by the account manager ----------------
INSERT INTO tasks (client_name, lead_time, product_name, product_quantity, supplement_name, supplement_quantity) 
VALUES ('Petr Ivanov', '2020-08-15 17:00:00', 'Pizza', 1, 'Tomatoes', 1),
('Tatiana Kirillova', '2020-08-15 17:30:00', 'Big Mac', 3, 'Mushrooms', 1),
('Julia Chaplian', '2020-08-15 17:00:00', 'Hamburger', 1, 'Cheese', 2),
('Petr Ivanov', '2020-08-15 17:00:00', 'Pizza', 1, 'Mayonnaise', 1),
('Petr Ivanov', '2020-08-15 17:00:00', 'Big Mac', 1, 'Tomatoes', 1),
('Julia Chaplian', '2020-08-15 22:00:00', 'Hamburger', 1, 'Cheese', 1);

--------------------- To be completed by the kitchen manager -------------
UPDATE tasks SET worker_name='Elena Zorina' WHERE number=1;
UPDATE tasks SET worker_name='Ksusha Lunina' WHERE number=2;
UPDATE tasks SET worker_name='Elena Zorina' WHERE number=3;
UPDATE tasks SET worker_name='Ksusha Lunina' WHERE number=4;
UPDATE tasks SET worker_name='Maria Shatohina' WHERE number=5;
UPDATE tasks SET worker_name='Kirill Zabaranilov' WHERE number=6;

UPDATE tasks SET completed=true WHERE number=5;
UPDATE tasks SET completed=true WHERE number=6;

--------------------- To be completed by the delivery service -------------
UPDATE tasks SET delivered=true WHERE number=6;
UPDATE tasks SET delivered=true WHERE number=4; -- The trigger will not let you set delivered = true if completed = false

------------ All orders ---------
--SELECT * FROM tasks;

----------- Information about the stage of completion of the 1st order ------------
SELECT * FROM tasks t LEFT JOIN clients c ON t.client_name=c.full_name WHERE client_name='Petr Ivanov';

------------- Information on the presence of allergens in the order -----------
--SELECT t.number, t.client_name, p.product_name, p.allergen, s.supplement_name, s.allergen FROM tasks t 
--LEFT JOIN products p ON t.product_name=p.product_name
--LEFT JOIN supplements s ON t.supplement_name=s.supplement_name
--WHERE client_name='Petr Ivanov';

